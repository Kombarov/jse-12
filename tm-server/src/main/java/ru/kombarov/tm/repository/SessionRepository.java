package ru.kombarov.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.session.ISessionRepository;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.enumerated.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository implements ISessionRepository {

    @Getter
    @NotNull
    private final Connection connection;

    public SessionRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @Override
    public Session fetch(final @Nullable ResultSet row) throws Exception {
        if (row == null) return null;
        final @NotNull Session session = new Session();
        session.setId(row.getString("id"));
        session.setUserId(row.getString("user_id"));
        session.setRole(Role.valueOf(row.getString("role")));
        session.setTimestamp(row.getTimestamp("timestamp").getTime());
        session.setSignature(row.getString("signature"));
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        final  String query = "select * from task-manager.app_session";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Session> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Nullable
    @Override
    public Session findOne(final @NotNull String id) throws Exception {
        final @NotNull String query = "select * from task-manager.app_session where id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable Session session = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return session;
    }

    @NotNull
    @Override
    public Session persist(final @NotNull Session session) throws Exception {
        final @NotNull String query = "insert into task-manager.app_session value(?, ?, ?, ?, ?)";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, session.getId());
        preparedStatement.setString(2, session.getUserId());
        preparedStatement.setTimestamp(3, new Timestamp(session.getTimestamp()));
        preparedStatement.setString(4, session.getSignature());
        preparedStatement.setString(5, String.valueOf(session.getRole()));
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return session;
    }

    @NotNull
    @Override
    public List<Session> persist(final @NotNull List<Session> sessions) throws Exception {
        for(final @NotNull Session session : sessions) {
            final @NotNull String query = "insert into task-manager.app_session value(?, ?, ?, ?, ?)";
            final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, session.getId());
            preparedStatement.setString(2, session.getUserId());
            preparedStatement.setTimestamp(3, new Timestamp(session.getTimestamp()));
            preparedStatement.setString(4, session.getSignature());
            preparedStatement.setString(5, String.valueOf(session.getRole()));
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return sessions;
    }

    @NotNull
    @Override
    public Session merge(final @NotNull Session session) throws Exception {
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement("update task-manager.app_session set user_id=?," +
                " timestamp=?, role=?, signature=? where id=?");
        preparedStatement.setString(1, session.getUserId());
        preparedStatement.setTimestamp(2, new Timestamp(session.getTimestamp()));
        preparedStatement.setString(3, String.valueOf(session.getRole()));
        preparedStatement.setString(4, session.getSignature());
        preparedStatement.setString(5, session.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return session;
    }

    @Override
    public void remove(final @NotNull String id) throws Exception {
        final @NotNull String query = "delete from task-manager.app_session where id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull String query = "delete from task-manager.app_session";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Nullable
    @Override
    public Session findOne(final @NotNull String userId, final @NotNull String id) throws Exception {
        final @NotNull String query = "select * from task-manager.app_session where user_id=? and id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable Session session = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return session;
    }

    @Override
    public void remove(final @NotNull String userId, final @NotNull String id) throws Exception {
        final @NotNull String query = "delete from task-manager.app_session where user_id=? and id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        preparedStatement.execute();
        preparedStatement.close();
    }
}
