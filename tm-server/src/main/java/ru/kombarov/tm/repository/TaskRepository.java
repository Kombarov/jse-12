package ru.kombarov.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.task.ITaskRepository;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static ru.kombarov.tm.util.date.DateUtil.parseDateToSQLDate;

public final class TaskRepository implements ITaskRepository {

    @Getter
    @NotNull
    private final Connection connection;

    public TaskRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @Override
    public Task fetch(final @Nullable ResultSet row) throws Exception {
        if (row == null) return null;
        final @NotNull Task task = new Task();
        task.setId(row.getString("id"));
        task.setProjectId(row.getString("project_id"));
        task.setUserId(row.getString("user_id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setStatus(Status.valueOf(row.getString("status")));
        task.setDateStart(row.getDate("dateStart"));
        task.setDateFinish(row.getDate("dateFinish"));
        return task;
    }

    @NotNull
    @Override
    public Task persist(final @NotNull Task task) throws Exception {
        final @NotNull String query = "insert into task-manager.app_task values (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, task.getId());
        preparedStatement.setString(2, task.getProjectId());
        preparedStatement.setString(3, task.getUserId());
        preparedStatement.setString(4, task.getName());
        preparedStatement.setString(5, task.getDescription());
        preparedStatement.setDate(6, parseDateToSQLDate(task.getDateStart()));
        preparedStatement.setDate(7, parseDateToSQLDate(task.getDateFinish()));
        preparedStatement.setString(8, String.valueOf(task.getStatus()));
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> persist(final @NotNull List<Task> tasks) throws Exception {
        for (final @NotNull Task task : tasks) {
            final @NotNull String query = "insert into task-manager.app_task values (?, ?, ?, ?, ?, ?, ?, ?)";
            final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, task.getId());
            preparedStatement.setString(2, task.getProjectId());
            preparedStatement.setString(3, task.getUserId());
            preparedStatement.setString(4, task.getName());
            preparedStatement.setString(5, task.getDescription());
            preparedStatement.setDate(6, parseDateToSQLDate(task.getDateStart()));
            preparedStatement.setDate(7, parseDateToSQLDate(task.getDateFinish()));
            preparedStatement.setString(8, String.valueOf(task.getStatus()));
            preparedStatement.close();
        }
        return tasks;
    }

    @NotNull
    @Override
    public Task merge(final @NotNull Task task) throws Exception {
        final @NotNull String query = "update task-manager.app_task set user_id=?, project_id=?," +
                " name=?, description=?, dateStart=?, dateFinish=?, status=? where id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, task.getUserId());
        preparedStatement.setString(2, task.getProjectId());
        preparedStatement.setString(2, task.getName());
        preparedStatement.setString(3, task.getDescription());
        preparedStatement.setDate(4, parseDateToSQLDate(task.getDateStart()));
        preparedStatement.setDate(5, parseDateToSQLDate(task.getDateFinish()));
        preparedStatement.setString(6, String.valueOf(task.getStatus()));
        preparedStatement.setString(7, task.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        final @NotNull String query = "select * from task-manager.app_task";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Nullable
    @Override
    public Task findOne(final @NotNull String id) throws Exception {
        final @NotNull String query = "select * from task-manager.app_task where id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable Task task = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return task;
    }

    @Override
    public void remove(final @NotNull String id) throws Exception {
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement("delete from task-manager.app_task where id=?");
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement("delete from task-manager.app_task");
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_task where user_id =?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Nullable
    @Override
    public Task findOne(final @NotNull String userId, final @NotNull String id) throws Exception {
        final @NotNull String query = "select * from task-manager.app_task where user_id=? and id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable Task task = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return task;
    }

    @Override
    public void removeAll(final @NotNull String userId) throws Exception {
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement("delete from task-manager.app_task where user_id=?");
        preparedStatement.setString(1, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Nullable
    @Override
    public String getIdByName(final @NotNull String name) throws Exception {
        final @NotNull String query = "select * from task-manager.app_task where name=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, name);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable Task task = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        if (task == null) return null;
        return task.getId();
    }

    @NotNull
    @Override
    public List<Task> getTasksByProjectId(final @NotNull String projectId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_task where project_id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, projectId);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Task> sortByDateStart(final @NotNull String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_task where user_id=? order by dateStart";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @NotNull
    @Override
    public  List<Task> sortByDateFinish(final @NotNull String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_task where user_id=? order by dateFinish";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Task> sortByStatus(final @NotNull String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_task where user_id=? order by FIELD(status, 'PLANNED', 'IN_PROCESS', 'DONE')";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Task> findByPart(final @NotNull String part, final @NotNull String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_task where user_id=? and description=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, part);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }
}
