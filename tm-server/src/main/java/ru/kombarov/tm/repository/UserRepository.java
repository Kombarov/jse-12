package ru.kombarov.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.user.IUserRepository;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.enumerated.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    @Getter
    @NotNull
    private final Connection connection;

    public UserRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @Override
    public User fetch(final @Nullable ResultSet row) throws Exception {
        if (row == null) return null;
        final @NotNull User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPassword(row.getString("password"));
        user.setRole(Role.valueOf(row.getString("role")));
        return user;
    }

    @NotNull
    @Override
    public User persist(final @NotNull User user) throws Exception {
        final @NotNull String query = "insert into task-manager.app_user values (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.setString(4, String.valueOf(user.getRole()));
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> persist(final @NotNull List<User> users) throws Exception {
        for(final @NotNull User user : users) {
            final @NotNull String query = "insert into task-manager.app_user values (?, ?, ?, ?)";
            final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, user.getId());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, String.valueOf(user.getRole()));
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return users;
    }

    @NotNull
    @Override
    public User merge(final @NotNull User user) throws Exception {
        final @NotNull String query = "update task-manager.app_user set login=?, password=?, role=? where id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, String.valueOf(user.getRole()));
        preparedStatement.setString(4, user.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        final @NotNull String query = "select * from task-manager.app_user";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<User> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Nullable
    @Override
    public User findOne(final @NotNull String id) throws Exception {
        final @NotNull String query = "select * from task-manager.app_user where id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable User user = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return user;
    }

    @Override
    public void remove(final @NotNull String id) throws Exception {
        final @NotNull String query = "delete from task-manager.app_user where id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull String query = "delete from task-manager.app_user";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Nullable
    @Override
    public String getIdByName(final @NotNull String name) throws Exception {
        final @NotNull String query = "select * from task-manager.app_user where login=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, name);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable User user = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        if (user == null) return null;
        return user.getId();
    }
}
