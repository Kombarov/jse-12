package ru.kombarov.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.project.IProjectRepository;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static ru.kombarov.tm.util.date.DateUtil.parseDateToSQLDate;

public final class ProjectRepository implements IProjectRepository {

    @Getter
    @NotNull
    private final Connection connection;

    public ProjectRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @Nullable
    @Override
    public Project fetch(final @Nullable ResultSet row) throws Exception {
        if (row == null) return null;
        final @NotNull Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(Status.valueOf(row.getString("status")));
        project.setDateStart(row.getDate("dateStart"));
        project.setDateFinish(row.getDate("dateFinish"));
        return project;
    }

    @NotNull
    @Override
    public Project persist(final @NotNull Project project) throws Exception {
        final @NotNull String query = "insert into task-manager.app_project values (?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, project.getId());
        preparedStatement.setString(2, project.getUserId());
        preparedStatement.setString(3, project.getName());
        preparedStatement.setString(4, project.getDescription());
        preparedStatement.setDate(5, parseDateToSQLDate(project.getDateStart()));
        preparedStatement.setDate(6, parseDateToSQLDate(project.getDateFinish()));
        preparedStatement.setString(7, String.valueOf(project.getStatus()));
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> persist(final @NotNull List<Project> projects) throws Exception {
        for (final @NotNull Project project : projects) {
            final @NotNull String query = "insert into task-manager.app_project values (?, ?, ?, ?, ?, ?, ?)";
            final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, project.getId());
            preparedStatement.setString(2, project.getUserId());
            preparedStatement.setString(3, project.getName());
            preparedStatement.setString(4, project.getDescription());
            preparedStatement.setDate(5, parseDateToSQLDate(project.getDateStart()));
            preparedStatement.setDate(6, parseDateToSQLDate(project.getDateFinish()));
            preparedStatement.setString(7, String.valueOf(project.getStatus()));
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return projects;
    }

    @NotNull
    @Override
    public Project merge(final @NotNull Project project) throws Exception {
        final @NotNull String query = "update task-manager.app_project set user_id=?," +
                " name=?, description=?, dateStart=?, dateFinish=?, status=? where id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, project.getUserId());
        preparedStatement.setString(2, project.getName());
        preparedStatement.setString(3, project.getDescription());
        preparedStatement.setDate(4, parseDateToSQLDate(project.getDateStart()));
        preparedStatement.setDate(5, parseDateToSQLDate(project.getDateFinish()));
        preparedStatement.setString(6, String.valueOf(project.getStatus()));
        preparedStatement.setString(7, project.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        final @NotNull String query = "select * from task-manager.app_project";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Nullable
    @Override
    public Project findOne(final @NotNull String id) throws Exception {
        final @NotNull String query = "select * from task-manager.app_project where id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return project;
    }

    @Override
    public void remove(final @NotNull String id) throws Exception {
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement("delete from task-manager.app_project where id=?");
        preparedStatement.setString(1, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement("delete from task-manager.app_project");
        preparedStatement.execute();
        preparedStatement.close();
    }

    @NotNull
    @Override
    public List<Project> findAll(final @NotNull String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_project where user_id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Nullable
    @Override
    public Project findOne(final @NotNull String userId, final @NotNull String id) throws Exception {
        final @NotNull String query = "select * from task-manager.app_project where user_id=? and id=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return project;
    }

    @Override
    public void removeAll(final @NotNull String userId) throws Exception {
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement("delete from task-manager.app_project where user_id=?");
        preparedStatement.setString(1, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Nullable
    @Override
    public String getIdByName(final @NotNull String name) throws Exception {
        final @NotNull String query = "select * from task-manager.app_project where name=?";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, name);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        final @Nullable Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        if (project == null) return null;
        return project.getId();
    }

    @NotNull
    @Override
    public List<Project> sortByDateStart(final @NotNull String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_project where user_id=? order by dateStart";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @NotNull
    @Override
    public  List<Project> sortByDateFinish(final @NotNull String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_project where user_id=? order by dateFinish";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Project> sortByStatus(final @NotNull String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_project where user_id=? order by FIELD(status, 'PLANNED', 'IN_PROCESS', 'DONE')";
        final @NotNull PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @NotNull
    @Override
    public List<Project> findByPart(final @NotNull String part, final @NotNull String userId) throws Exception {
        final @NotNull String query = "select * from task-manager.app_project where user_id=? and description=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, part);
        final @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        final @NotNull List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }
}
