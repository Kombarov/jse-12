package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.project.IProjectService;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.repository.ProjectRepository;
import ru.kombarov.tm.util.database.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    @Override
    public Connection getConnection() throws Exception {
        return ConnectionUtil.getConnection();
    }

    @NotNull
    @Override
    public Project persist(final @Nullable Project project) throws Exception {
        if(project == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).persist(project);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public List<Project> persist(@NotNull final List<Project> projects) throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).persist(projects);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return projects;
    }


    @NotNull
    @Override
    public Project merge(final @Nullable Project project) throws Exception {
        if(project == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).merge(project);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findAll();
    }

    @Nullable
    @Override
    public Project findOne(final @Nullable String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findOne(id);
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if(id == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).remove(id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).removeAll();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    @Nullable
    public String getIdByName(final @Nullable String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).getIdByName(name);
    }

    @NotNull
    @Override
    public List<Project> findAll(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findAll(userId);
    }

    @Nullable
    @Override
    public Project findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findOne(userId, id);
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).removeAll(userId);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<Project> sortByDateStart(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).sortByDateStart(userId);
    }

    @NotNull
    @Override
    public List<Project> sortByDateFinish(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).sortByDateFinish(userId);
    }

    @NotNull
    @Override
    public List<Project> sortByStatus(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).sortByStatus(userId);
    }

    @NotNull
    @Override
    public List<Project> findByPart(final @Nullable String part, final @Nullable String userId) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findByPart(part, userId);
    }
}