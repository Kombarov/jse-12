package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.user.IUserService;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.repository.UserRepository;
import ru.kombarov.tm.util.database.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    @Override
    public Connection getConnection() throws Exception {
        return ConnectionUtil.getConnection();
    }

    @NotNull
    @Override
    public User persist(final @Nullable User user) throws Exception {
        if(user == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).persist(user);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public List<User> persist(final @NotNull List<User> users) throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).persist(users);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return users;
    }

    @NotNull
    @Override
    public User merge(final @Nullable User user) throws Exception {
        if(user == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).merge(user);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new UserRepository(connection).findAll();
    }

    @Nullable
    @Override
    public User findOne(final @Nullable String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new UserRepository(connection).findOne(id);
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).remove(id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).removeAll();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public String getIdByName(final @Nullable String login) throws Exception {
        if(login == null || login.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new UserRepository(connection).getIdByName(login);
    }
}