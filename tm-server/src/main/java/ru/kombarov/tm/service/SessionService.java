package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.session.ISessionService;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.repository.SessionRepository;
import ru.kombarov.tm.util.database.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    @Override
    public Connection getConnection() throws Exception {
        return ConnectionUtil.getConnection();
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new SessionRepository(connection).findAll();
    }

    @Nullable
    @Override
    public Session findOne(final @Nullable String id) throws Exception {
        if(id == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new SessionRepository(connection).findOne(id);
    }

    @NotNull
    @Override
    public Session persist(final @Nullable Session session) throws Exception {
        if(session == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).persist(session);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return session;
    }

    @NotNull
    @Override
    public List<Session> persist(final @NotNull List<Session> sessions) throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).persist(sessions);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return sessions;
    }

    @NotNull
    @Override
    public Session merge(final @Nullable Session session) throws Exception {
        if(session == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).merge(session);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return session;
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if(id == null) throw new Exception();
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).remove(id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).removeAll();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public Session findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        if(id == null || id.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new SessionRepository(connection).findOne(userId, id);
    }

    @Override
    public void remove(final @Nullable String userId, final @Nullable String id) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        if(id == null || id.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).remove(userId, id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }
}
