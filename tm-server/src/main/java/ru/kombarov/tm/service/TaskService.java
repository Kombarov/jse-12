package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.task.ITaskService;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.repository.TaskRepository;
import ru.kombarov.tm.util.database.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    @Override
    public Connection getConnection() throws Exception {
        return ConnectionUtil.getConnection();
    }

    @NotNull
    @Override
    public Task persist(final @Nullable Task task) throws Exception {
        if(task == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).persist(task);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    public List<Task> persist(final @NotNull List<Task> tasks) throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).persist(tasks);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return tasks;
    }

    @NotNull
    @Override
    public Task merge(final @Nullable Task task) throws Exception {
        if(task == null) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).merge(task);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findAll();
    }

    @Nullable
    @Override
    public Task findOne(final @Nullable String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findOne(id);
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).remove(id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).removeAll();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public String getIdByName(final @Nullable String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).getIdByName(name);
    }

    @NotNull
    @Override
    public List<Task> getTasksByProjectId(final @Nullable String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).getTasksByProjectId(projectId);
    }

    @NotNull
    @Override
    public List<Task> findAll(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findAll(userId);
    }

    @Nullable
    @Override
    public Task findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findOne(userId, id);
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).removeAll(userId);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<Task> sortByDateStart(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).sortByDateStart(userId);
    }

    @NotNull
    @Override
    public List<Task> sortByDateFinish(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).sortByDateFinish(userId);
    }

    @NotNull
    @Override
    public List<Task> sortByStatus(final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).sortByStatus(userId);
    }

    @NotNull
    @Override
    public List<Task> findByPart(final @Nullable String part, final @Nullable String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        if(part == null || part.isEmpty()) throw new Exception();
        final @NotNull Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findByPart(part, userId);
    }
}