package ru.kombarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.IProjectEndpoint;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.enumerated.Status;
import ru.kombarov.tm.service.ProjectService;
import ru.kombarov.tm.service.SessionService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kombarov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private ProjectService projectService;

    public ProjectEndpoint() {
        super();
    }

    public ProjectEndpoint(final @NotNull SessionService sessionService, final @NotNull ProjectService projectService) {
        super(sessionService);
        this.projectService = projectService;
    }

    @Override
    @WebMethod
    public void persistProject(final @Nullable Session session, final @Nullable Project project) throws Exception {
        validateSession(session);
        projectService.persist(project);
    }

    @Override
    @WebMethod
    public void persistProjects(final @Nullable Session session, final @Nullable List<Project> projects) throws Exception {
        validateSession(session);
        projectService.persist(projects);
    }

    @Override
    @WebMethod
    public void mergeProject(final @Nullable Session session, final @Nullable Project project) throws Exception {
        validateSession(session);
        projectService.merge(project);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjects(final @Nullable Session session) throws Exception {
        validateSession(session);
        return projectService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProject(final @Nullable Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        return projectService.findOne(id);
    }

    @Override
    @WebMethod
    public void removeProject(final @Nullable Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        projectService.remove(id);
    }

    @Override
    @WebMethod
    public void removeAllProjects(final @Nullable Session session) throws Exception {
        validateSession(session);
        projectService.removeAll();
    }

    @Nullable
    @WebMethod
    public String getIdByProjectName(final @Nullable Session session, final @Nullable String name) throws Exception {
        validateSession(session);
        return projectService.getIdByName(name);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> getProjectListByUserId(final @Nullable Session session) throws Exception {
        validateSession(session);
        return projectService.findAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjectsByUserId(final @Nullable Session session) throws Exception {
        validateSession(session);
        return projectService.findAll(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProjectByUserId(final @Nullable Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        return projectService.findOne(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllProjectsByUserId(final @Nullable Session session) throws Exception {
        validateSession(session);
        projectService.removeAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> sortProjectsByDateStart(final @Nullable Session session) throws Exception {
        validateSession(session);
        return projectService.sortByDateStart(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> sortProjectsByDateFinish(final @Nullable Session session) throws Exception {
        validateSession(session);
        return projectService.sortByDateFinish(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> sortProjectsByStatus(final @Nullable Session session) throws Exception {
        validateSession(session);
        return projectService.sortByStatus(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findProjectsByPart(final @Nullable Session session, final @Nullable String part) throws Exception {
        validateSession(session);
        return projectService.findByPart(part, session.getUserId());
    }
}
