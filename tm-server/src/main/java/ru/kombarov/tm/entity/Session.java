package ru.kombarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractEntity {

    @NotNull
    String userId;

    @NotNull
    Role role;

    @NotNull
    Long timestamp = new Date().getTime();

    @Nullable
    String signature;
}
