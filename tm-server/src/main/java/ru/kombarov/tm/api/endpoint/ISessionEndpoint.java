package ru.kombarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    Session createSession(final @NotNull String login, final @NotNull String password) throws Exception;

    @WebMethod
    void removeSession(final @Nullable String userId, final @Nullable String id) throws Exception;
}
