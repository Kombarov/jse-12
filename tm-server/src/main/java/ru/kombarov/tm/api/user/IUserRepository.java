package ru.kombarov.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

public interface IUserRepository {

    @NotNull
    Connection getConnection();

    @Nullable
    User fetch(final @Nullable ResultSet row) throws Exception;

    @NotNull
    User persist(final @NotNull User user) throws Exception;

    @NotNull
    List<User> persist(final @NotNull List<User> users) throws Exception;

    @NotNull
    User merge(final @NotNull User user) throws Exception;

    @NotNull
    List<User> findAll() throws Exception;

    @Nullable
    User findOne(final @NotNull String id) throws Exception;

    void remove(final @NotNull String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    String getIdByName(final @NotNull String name) throws Exception;
}
