package ru.kombarov.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Project;

import java.sql.Connection;
import java.util.List;

public interface IProjectService {

    @NotNull
    Connection getConnection() throws Exception;

    @NotNull
    Project persist(final @Nullable Project project) throws Exception;

    @NotNull
    List<Project> persist(@NotNull final List<Project> projects) throws Exception;


    @NotNull
    Project merge(final @Nullable Project project) throws Exception;

    @NotNull
    List<Project> findAll() throws Exception;

    @Nullable
    Project findOne(final @Nullable String id) throws Exception;

    void remove(final @Nullable String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    String getIdByName(final @Nullable String name) throws Exception;

    @NotNull
    List<Project> findAll(final @Nullable String userId) throws Exception;

    @Nullable
    Project findOne(final @Nullable String userId, final @Nullable String id) throws Exception;

    void removeAll(final @Nullable String userId) throws Exception;

    @NotNull
    List<Project> sortByDateStart(final @Nullable String userId) throws Exception;

    @NotNull
    List<Project> sortByDateFinish(final @Nullable String userId) throws Exception;

    @NotNull
    List<Project> sortByStatus(final @Nullable String userId) throws Exception;

    @NotNull
    List<Project> findByPart(final @Nullable String part, final @Nullable String userId) throws Exception;
}
