package ru.kombarov.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

public interface ISessionRepository {

    @NotNull
    Connection getConnection();

    @Nullable
    Session fetch(final @Nullable ResultSet row) throws Exception;

    @NotNull
    List<Session> findAll() throws Exception;

    @Nullable
    Session findOne(final @NotNull String id) throws Exception;

    @NotNull
    Session persist(final @NotNull Session session) throws Exception;

    @NotNull
    List<Session> persist(final @NotNull List<Session> sessions) throws Exception;

    @NotNull
    Session merge(final @NotNull Session session) throws Exception;

    void remove(final @NotNull String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    Session findOne(final @NotNull String userId, final @NotNull String id) throws Exception;

    void remove(final @NotNull String userId, final @NotNull String id) throws Exception;
}
