package ru.kombarov.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Task;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

public interface ITaskRepository {

    @NotNull
    Connection getConnection();

    @Nullable
    Task fetch(final @Nullable ResultSet row) throws Exception;

    @NotNull
    Task persist(final @NotNull Task task) throws Exception;

    @NotNull
    List<Task> persist(final @NotNull List<Task> tasks) throws Exception;

    @NotNull
    Task merge(final @NotNull Task task) throws Exception;

    @NotNull
    List<Task> findAll() throws Exception;

    @Nullable
    Task findOne(final @NotNull String id) throws Exception;

    void remove(final @NotNull String id) throws Exception;

    void removeAll() throws Exception;

    @NotNull
    List<Task> findAll(@NotNull final String userId) throws Exception;

    @Nullable
    Task findOne(final @NotNull String userId, final @NotNull String id) throws Exception;

    void removeAll(final @NotNull String userId) throws Exception;

    @Nullable
    String getIdByName(final @NotNull String name) throws Exception;

    @NotNull
    List<Task> getTasksByProjectId(final @NotNull String projectId) throws Exception;

    @NotNull
    List<Task> sortByDateStart(final @NotNull String userId) throws Exception;

    @NotNull
    List<Task> sortByDateFinish(final @NotNull String userId) throws Exception;

    @NotNull
    List<Task> sortByStatus(final @NotNull String userId) throws Exception;

    @NotNull
    List<Task> findByPart(final @NotNull String part, final @NotNull String userId) throws Exception;
}
