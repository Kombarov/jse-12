package ru.kombarov.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;

import java.sql.Connection;
import java.util.List;

public interface ISessionService {

    @NotNull
    Connection getConnection() throws Exception;

    @NotNull
    List<Session> findAll() throws Exception;

    @Nullable
    Session findOne(final @Nullable String id) throws Exception;

    @NotNull
    Session persist(final @Nullable Session session) throws Exception;

    @NotNull
    List<Session> persist(final @NotNull List<Session> sessions) throws Exception;

    @NotNull
    Session merge(final @Nullable Session session) throws Exception;

    void remove(final @Nullable String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    Session findOne(final @Nullable String userId, final @Nullable String id) throws Exception;

    void remove(final @Nullable String userId, final @Nullable String id) throws Exception;
}
