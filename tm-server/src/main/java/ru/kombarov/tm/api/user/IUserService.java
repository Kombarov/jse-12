package ru.kombarov.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.User;

import java.sql.Connection;
import java.util.List;

public interface IUserService {

    @NotNull
    Connection getConnection() throws Exception;

    @NotNull
    User persist(final @Nullable User user) throws Exception;

    @NotNull
    List<User> persist(final @NotNull List<User> users) throws Exception;

    @NotNull
    User merge(final @Nullable User user) throws Exception;

    @NotNull
    List<User> findAll() throws Exception;

    @Nullable
    User findOne(final @Nullable String id) throws Exception;

    void remove(final @Nullable String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    String getIdByName(final @Nullable String login) throws Exception;
}
