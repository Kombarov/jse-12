package ru.kombarov.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Project;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

public interface IProjectRepository {

    @NotNull
    Connection getConnection();

    @Nullable
    Project fetch(final @Nullable ResultSet row) throws Exception;

    @NotNull
    Project persist(final @NotNull Project project) throws Exception;

    @NotNull
    List<Project> persist(final @NotNull List<Project> projects) throws Exception;

    @NotNull
    Project merge(final @NotNull Project project) throws Exception;

    @NotNull
    List<Project> findAll() throws Exception;

    @Nullable
    Project findOne(final @NotNull String id) throws Exception;

    void remove(final @NotNull String id) throws Exception;

    void removeAll() throws Exception;

    @NotNull
    List<Project> findAll(final @NotNull String userId) throws Exception;

    @Nullable
    Project findOne(final @NotNull String userId, final @NotNull String id) throws Exception;

    void removeAll(final @NotNull String userId) throws Exception;

    @Nullable
    String getIdByName(final @NotNull String name) throws Exception;

    @NotNull
    List<Project> sortByDateStart(final @NotNull String userId) throws Exception;

    @NotNull
     List<Project> sortByDateFinish(final @NotNull String userId) throws Exception;

    @NotNull
    List<Project> sortByStatus(final @NotNull String userId) throws Exception;

    @NotNull
    List<Project> findByPart(final @NotNull String part, final @NotNull String userId) throws Exception;
}
