package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.Task;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskUnattachCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-unattach";
    }

    @NotNull
    @Override
    public String description() {
        return "Unattach task from the project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UNATTACH]");
        if (serviceLocator == null) throw new Exception();
        printTasks(serviceLocator.getTaskEndpoint().findAllTasksByUserId(serviceLocator.getSession()));
        System.out.println("ENTER TASK NAME");
        final @Nullable String taskName = input.readLine();
        final @Nullable String taskId = serviceLocator.getTaskEndpoint().getIdByTaskName(serviceLocator.getSession(), taskName);
        final @Nullable Task task = serviceLocator.getTaskEndpoint().findOneTask(serviceLocator.getSession(), taskId);
        if (task != null) task.setProjectId("");
        System.out.println("[OK]");
    }
}
