package ru.kombarov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Print build information.";
    }

    @Override
    public void execute() {
        String developer = Manifests.read("developer");
        String createdBy = Manifests.read("Created-By");
        String buildJdk = Manifests.read("Build-Jdk");
        String buildNumber = Manifests.read("buildNumber");
        System.out.println("Developer: " + developer);
        System.out.println("Created by: " + createdBy);
        System.out.println("Build JDK: " + buildJdk);
        System.out.println("Build number: " + buildNumber);
    }
}
