package ru.kombarov.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class DataJaxBJSONSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data JaxB json save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data by JaxB to JSON format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB JSON SAVE]");
        if (serviceLocator == null) return;
        System.out.println("[OK]");
    }
}
