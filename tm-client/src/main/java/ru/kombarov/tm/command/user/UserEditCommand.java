package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.api.endpoint.User;
import ru.kombarov.tm.command.AbstractCommand;

public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit user.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER EDIT]");
        if (serviceLocator == null) throw new Exception();
        System.out.println("ENTER NEW USERNAME");
        @NotNull final User user = new User();
        user.setId(serviceLocator.getSession().getUserId());
        user.setLogin(input.readLine());
        System.out.println("ENTER NEW PASSWORD");
        user.setPassword(input.readLine());
        serviceLocator.getUserEndpoint().mergeUser(serviceLocator.getSession(), user);
        System.out.println("[OK]");
    }
}
